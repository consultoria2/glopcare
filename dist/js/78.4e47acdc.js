"use strict";(self["webpackChunkglopcare"]=self["webpackChunkglopcare"]||[]).push([[78],{6078:function(e,t,n){n.r(t),n.d(t,{startStatusTap:function(){return a}});var r=n(65),o=n(8487),s=n(6587);
/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */
const a=()=>{const e=window;e.addEventListener("statusTap",(()=>{(0,r.wj)((()=>{const t=e.innerWidth,n=e.innerHeight,a=document.elementFromPoint(t/2,n/2);if(!a)return;const c=(0,o.a)(a);c&&new Promise((e=>(0,s.c)(c,e))).then((()=>{(0,r.Iu)((async()=>{c.style.setProperty("--overflow","hidden"),await(0,o.s)(c,300),c.style.removeProperty("--overflow")}))}))}))}))}}}]);
//# sourceMappingURL=78.4e47acdc.js.map