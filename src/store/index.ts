import {createStore} from 'vuex';
 
const store = createStore({
    state() {
        return {
            count: 0,
            selectedTemplate: null,
            user: null,
            access_token: null,
            account_option: null,
            uid: null,
            color_palette: null,
            product_or_service: null,
            available_groups: [],
            selected_group: null
        }
    },
    mutations: {
        setSelectedTemplate(state: any, payload) {
            state.selectedTemplate = payload;
        },
        setUser(state: any, payload){
            state.user = payload;
        },
        setAccessToken(state, payload) {
            state.access_token = payload;
        },
        setAccountOption(state, payload) {
            state.account_option = payload;
        },
        setUID(state, payload) {
            state.uid = payload;
        },
        setColorPalette(state, payload) {
            state.color_palette = payload;
        },
        setProductOrService(state, payload) {
            state.product_or_service = payload;
        },
        setAvailableGroups(state, payload) {
            state.available_groups = payload;
        },
        setSelectedGroup(state, payload) {
            state.selected_group = payload;
        },
    },
    getters: {
        getSelectedTemplate(state: any) {
            return state.selectedTemplate;
        },
        getUser(state: any){
            return state.user;
        },
        getAccessToken(state) {
            return state.access_token;
        },
        getAccountOption(state) {
            return state.account_option;
        },
        getUID(state) {
            return state.uid;
        },
        getColorPalette(state) {
            return state.color_palette;
        },
        getProductOrService(state) {
            return state.product_or_service;
        },
        getAvailableGroups(state) {
            return state.available_groups;
        },
        getSelectedGroup(state) {
            return state.selected_group;
        },
    }
});
 
export default store
