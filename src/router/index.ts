import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import HomePage from '../views/HomePage.vue';
import HomeHelpPage from '../views/HomeHelpPage.vue';
import WelcomePage from '../views/WelcomePage.vue';
import SocialRegisterPage from '../views/SocialRegisterPage.vue';
import RegisterPage from '../views/RegisterPage.vue';
import CompleteRegisterPage from '../views/CompleteRegisterPage.vue';
import PhoneRegisterPage from '../views/PhoneRegisterPage.vue';
import LoginPage from '../views/LoginPage.vue';
import PhoneLoginPage from '../views/PhoneLoginPage.vue';
import LostPasswordPage from '../views/LostPasswordPage.vue';
import AccountOptionsPage from '../views/AccountOptionsPage.vue';
import SharePage from '../views/SharePage.vue';
import QRPage from '../views/QRPage.vue';
import PreviewPage from '../views/PreviewPage.vue';
import PreviewCatalogPage from '../views/PreviewCatalogPage.vue';
import GlopcardChooseTemplateStepPage from '../views/GlopcardChooseTemplateStepPage.vue';
import GlopcardSettingsStepPage from '../views/GlopcardSettingsStepPage.vue';
import GlopcardPicturesStepPage from '../views/GlopcardPicturesStepPage.vue';
import GlopcardBackgroundStepPage from '../views/GlopcardBackgroundStepPage.vue';
import GlopcardBackgroundColorStepPage from '../views/GlopcardBackgroundColorStepPage.vue';
import GlopcardTextColorStepPage from '../views/GlopcardTextColorStepPage.vue';
import GlopcardProfessionalInformationStepPage from '../views/GlopcardProfessionalInformationStepPage.vue';
import GlopcardContactInformationStepPage from '../views/GlopcardContactInformationStepPage.vue';
import GlopcardSocialNetworksStepPage from '../views/GlopcardSocialNetworksStepPage.vue';
import GlopcardIconsStepPage from '../views/GlopcardIconsStepPage.vue';
import GlopcardCalendlyStepPage from '../views/GlopcardCalendlyStepPage.vue';
import GlopcardLocationStepPage from '../views/GlopcardLocationStepPage.vue';
import GlopcardCatalogStepPage from '../views/GlopcardCatalogStepPage.vue';

import CatalogOptionsPage from '../views/CatalogOptionsPage.vue';
import EditProductOptionsPage from '../views/EditProductOptionsPage.vue';
import EditPriceListPage from '../views/EditPriceListPage.vue';
import EditProductPage from '../views/EditProductPage.vue';
import EditCategoryPage from '../views/EditCategoryPage.vue';
import EditGroupPage from '../views/EditGroupPage.vue';
import EditPaymentsMethodsPage from '../views/EditPaymentsMethodsPage.vue';
import DeleteAccountMessagePage from '../views/DeleteAccountMessagePage.vue';
import ListCatalogItemsPage from '../views/ListCatalogItemsPage.vue';
import ListPriceItemsPage from '../views/ListPriceItemsPage.vue';

import GlopcardCustomCardStepPage from '../views/GlopcardCustomCardStepPage.vue';

/* Mixins */
import api from "../api";
import store from "../store";

import { Storage } from "@ionic/storage";

import { Device } from '@capacitor/device';

import { alertController } from "@ionic/vue";

const storage = new Storage();

const routes: Array<RouteRecordRaw> = [  
  {
    path: '/',
    name: 'Welcome',
    component: WelcomePage
  },
  {
    path: '/register',
    name: 'Register',
    component: RegisterPage
  },
  {
    path: '/complete-register',
    name: 'Complete Register',
    component: CompleteRegisterPage,
    beforeEnter: (to, from) => {
      console.log('Watching Login (from, to)...');
      console.log(from);
      console.log(to);
      if (from && from.path && from.path.indexOf('home') > 0) {
        return false;
      }
    },
  },  
  {
    path: '/social-register',
    name: 'Social Register',
    component: SocialRegisterPage,
    beforeEnter: (to, from) => {
      console.log('Watching Login (from, to)...');
      console.log(from);
      console.log(to);
      if (from && from.path && from.path.indexOf('home') > 0) {
        return false;
      }
    },
  },
  {
    path: '/phone-register',
    name: 'Phone Register',
    component: PhoneRegisterPage
  },
  {
    path: '/login',
    name: 'Login',
    component: LoginPage,
    beforeEnter: (to, from) => {
      console.log('Watching Login (from, to)...');
      console.log(from);
      console.log(to);
      if (from && from.path && from.path.indexOf('home') > 0) {
        return false;
      }
    },
  },
  {
    path: '/phone-login',
    name: 'Phone Login',
    component: PhoneLoginPage
  },  
  {
    path: '/home',
    name: 'Home',
    component: HomePage
  },
  {
    path: '/home-help',
    name: 'HomeHelp',
    component: HomeHelpPage
  },
  {
    path: '/password',
    name: 'Password',
    component: LostPasswordPage
  },
  {
    path: '/share',
    name: 'Share',
    component: SharePage
  },
  {
    path: '/qr-share/:uid',
    name: 'QRPage',
    component: QRPage
  },
  {
    path: '/account-options',
    name: 'Account Options',
    component: AccountOptionsPage
  },
  {
    path: '/account-options-account-help',
    name: 'AccountOptionsAccountHelp',
    component: HomeHelpPage
  },
  {
    path: '/account-options-notifications-help',
    name: 'AccountOptionsNotificationsHelp',
    component: HomeHelpPage
  },
  {
    path: '/account-options-marketing-help',
    name: 'AccountOptionsMarketingHelp',
    component: HomeHelpPage
  },
  {
    path: '/account-options-customers-help',
    name: 'AccountOptionsCustomersHelp',
    component: HomeHelpPage
  },
  {
    path: '/account-options-settings-help',
    name: 'AccountOptionsSettingsHelp',
    component: HomeHelpPage
  },
  {
    path: '/preview',
    name: 'Preview',
    component: PreviewPage
  },
  {
    path: '/preview-catalog',
    name: 'PreviewCatalog',
    component: PreviewCatalogPage
  },  
  {
    path: '/glopcard-choose-template-step',
    name: 'Glopcard Choose Template Step',
    component: GlopcardChooseTemplateStepPage
  },
  {
    path: '/glopcard-choose-template-step-help',
    name: 'GlopcardChooseTemplateStepHelp',
    component: HomeHelpPage
  },
  {
    path: '/glopcard-settings-step',
    name: 'Glopcard Settings Step',
    component: GlopcardSettingsStepPage
  },
  {
    path: '/glopcard-settings-step-help',
    name: 'GlopcardSettingsStepHelp',
    component: HomeHelpPage
  },
  {
    path: '/glopcard-pictures-step',
    name: 'Glopcard Pictures Step',
    component: GlopcardPicturesStepPage
  },
  {
    path: '/glopcard-pictures-step-help',
    name: 'GlopcardPicturesStepHelp',
    component: HomeHelpPage
  },
  {
    path: '/glopcard-background-step',
    name: 'Glopcard Background Step',
    component: GlopcardBackgroundStepPage
  },
  {
    path: '/glopcard-background-step-help',
    name: 'Glopcard Background Step Help',
    component: HomeHelpPage
  },
  {
    path: '/glopcard-background-color-step',
    name: 'Glopcard Background Color Step',
    component: GlopcardBackgroundColorStepPage
  },
  {
    path: '/glopcard-background-color-step-help',
    name: 'Glopcard Background Color Step Help',
    component: HomeHelpPage
  },
  {
    path: '/glopcard-text-color-step',
    name: 'Glopcard Text Color Step',
    component: GlopcardTextColorStepPage
  },
  {
    path: '/glopcard-text-color-step-help',
    name: 'Glopcard Text Color Step Help',
    component: HomeHelpPage
  },
  {
    path: '/glopcard-professional-information-step',
    name: 'Glopcard Professional Information Step',
    component: GlopcardProfessionalInformationStepPage
  },
  {
    path: '/glopcard-professional-information-step-help',
    name: 'Glopcard Professional Information Step Help',
    component: HomeHelpPage
  },
  {
    path: '/glopcard-contact-information-step',
    name: 'Glopcard Contact Information Step',
    component: GlopcardContactInformationStepPage
  },
  {
    path: '/glopcard-contact-information-step-help',
    name: 'Glopcard Contact Information Step Help',
    component: HomeHelpPage
  },
  {
    path: '/glopcard-social-networks-step',
    name: 'Glopcard Social Networks Step',
    component: GlopcardSocialNetworksStepPage
  },
  {
    path: '/glopcard-social-networks-step-help',
    name: 'Glopcard Social Networks Step Help',
    component: HomeHelpPage
  },
  {
    path: '/glopcard-icons-step',
    name: 'Glopcard Icons Step',
    component: GlopcardIconsStepPage
  },
  {
    path: '/glopcard-icons-step-help',
    name: 'Glopcard Icons Step Help',
    component: HomeHelpPage
  },
  {
    path: '/glopcard-calendly-step',
    name: 'Glopcard Calendly Step',
    component: GlopcardCalendlyStepPage
  },
  {
    path: '/glopcard-location-step',
    name: 'Glopcard Location Step',
    component: GlopcardLocationStepPage
  },
  {
    path: '/glopcard-location-step-help',
    name: 'Glopcard Location Step Help',
    component: HomeHelpPage
  },
  {
    path: '/glopcard-calendly-step-help',
    name: 'Glopcard Calendly Step Help',
    component: HomeHelpPage
  },
  {
    path: '/glopcard-catalog-step',
    name: 'Glopcard Catalog Step',
    component: GlopcardCatalogStepPage
  },
  {
    path: '/glopcard-custom-card-step',
    name: 'Glopcard Custom Card Step',
    component: GlopcardCustomCardStepPage
  },
  {
    path: '/catalog-options',
    name: 'Catalog Options',
    component: CatalogOptionsPage
  },
  {
    path: '/edit-product-options',
    name: 'Edit Product Options',
    component: EditProductOptionsPage
  },
  {
    path: '/edit-price-list',
    name: 'Edit Price List',
    component: EditPriceListPage
  },
  {
    path: '/edit-product',
    name: 'Add Product',
    component: EditProductPage
  },
  {
    path: '/edit-product/:id',
    name: 'Edit Product',
    component: EditProductPage
  },
  {
    path: '/list-catalog-items',
    name: 'List Catalog Items Product',
    component: ListCatalogItemsPage
  },
  {
    path: '/list-price-items',
    name: 'List Price Items Product',
    component: ListPriceItemsPage
  },
  {
    path: '/list-catalog-items/archived',
    name: 'List Catalog Archived Items',
    component: ListCatalogItemsPage
  },
  {
    path: '/edit-categories',
    name: 'Edit Categories',
    component: EditCategoryPage
  },
  {
    path: '/edit-groups',
    name: 'Edit Groups',
    component: EditGroupPage
  },
  {
    path: '/payment-methods',
    name: 'Payments Methods',
    component: EditPaymentsMethodsPage
  },
  {
    path: '/delete-account-message',
    name: 'Delete Account Message',
    component: DeleteAccountMessagePage
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach(async (to, from) => {
  console.info('Navigation Guard...');
  console.info('To:', to, 'From:', from);

  const isAuth = false;

  try {
    await storage.get("user");
    await storage.get("accessToken");
  } catch (error) {
    console.log('Starting storage...');
    await storage.create();
  }

  const user = store.getters.getUser ? store.getters.getUser : await storage.get("user");
  const accessToken = store.getters.getAccessToken ? store.getters.getAccessToken : await storage.get("accessToken");

  const arrRoutes = ['Login', 'Register', 'Social Register', 'Phone Login', 'Phone Register', 'Welcome', 'Password'];

  if (user === null) {
    console.log("User is not set...");

    if (accessToken === null) {

      // Clearing storage and logging out
      console.log("Access token is not set. Logging out...");
      storage.clear();

      if (arrRoutes.includes(to.name as string)){
        return true;
      } else {
        return { name: 'Welcome' };
      }
      

    } else {
      // Access token exists
      console.log("Attemping sign in with access token...");
      store.commit("setAccessToken", accessToken);

      // Retrieve user
      await api
        .get("me", {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        })
        .then(async (response) => {
          console.log(response);

          const parsedValue = JSON.parse(JSON.stringify(response.data));
          storage.set("user", parsedValue);
          store.commit("setUser", parsedValue);

          console.log(`¡Bienvenido, ${response.data.name}!`);
        })
        .catch(function (error) {
          console.log(error);
          console.error(JSON.stringify(error.response.data));
        });
    }
  } else {
    console.log("User is set in storage");

    if (store.getters.getUser === null) {
      console.log("User is not set in store. Setting user...");
      store.commit("setUser", user);
    } else {
      console.log("User is set in store");
    }
    
    if (accessToken === null) {

      // Clearing storage and logging out
      console.log("Access token is not set. Logging out...");
      storage.clear();
      return { name: 'Welcome' };

    } else {

      console.log("Access token is set in storage");

      if (store.getters.getAccessToken === null) {
        console.log(`Commiting existing access token (${accessToken})...`);
        store.commit("setAccessToken", accessToken);
      } else {
        console.log("Access token is set in store");
      }

      // Initialize glopcard if it is does not exist

      if (user && user.profile && user.profile.status && user.profile.selected_template) {
        store.commit("setSelectedTemplate", user.profile.selected_template);
      } else {
        const initial = {
          status: 'started',
          last_step: 'glopcard-choose-template-step'
        }

        user.profile = initial;
        
        store.commit("setUser", user);
        storage.set("user", user);
      }

      // Check device ID

      const deviceId = await Device.getId();
      console.log('Device Id from Router', deviceId);

      const objDevice = {
        device_id: deviceId.uuid
      }

      await api
      .post("check-device-id", objDevice, {
        headers: {
          Authorization: `Bearer ${store.getters.getAccessToken}`,
        },
      })
        .then(async (response) => {
          console.log(response);
        })
        .catch(async function (error) {
          console.log(error);

          let message;

          if (error && error.response && error.response.status){
            switch (error.response.status) {
              case 403:
                message = error.response.data.message;
              break;
              default:
                message = error.response.message;
            }
          } else {
            message = error.message;
          }

          const alert = await alertController.create({
            header: "Alerta",
            subHeader: "Ha ocurrido un error.",
            message: message,
            buttons: [
              {
                text: "OK",
                role: "confirm",
                handler: async () => {
                  console.log("Logging out...");

                  const storage = new Storage();

                  await storage.create();
                  await storage.clear();

                  document.location.reload();
                },
              },
            ],
          });
          await alert.present();
        });

      // Redirect to complete register if is coming from social network and no username has been specified

      if (user.request_username && to.name != 'Complete Register') {
        return { name: 'Complete Register' };
      }

      if(arrRoutes.includes(to.name as string)) {
        return { name: 'Home' };
      } else {
        return true;
      }
      
    }
  }

  return isAuth;
})

export default router
