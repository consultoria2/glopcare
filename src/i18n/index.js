const messages = {
    en: {
      message: {
        complete_register: 'Complete your Registration'
      },
      button: {
        create_account: 'Create Account'
      }
    },
    es: {
      message: {
        complete_register: 'Completa tu Registro'
      },
      button: {
        create_account: 'Crear Cuenta'
      }
    }
  }

export default messages;