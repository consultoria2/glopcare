import * as firebase from 'firebase';
const config = {
  apiKey: "AIzaSyBdyLFeEeZ-oH5XWRIBk8HzU0LAPeCFe5c",
  authDomain: "glopcare-16d5f.firebaseapp.com",
  projectId: "glopcare-16d5f",
  storageBucket: "glopcare-16d5f.appspot.com",
  messagingSenderId: "163364824738",
  appId: "1:163364824738:web:96e94e675eed922df8d9fb",
  measurementId: "G-WK11LJ8FXG"
};
firebase.initializeApp(config);
firebase.getAnalytics(config);

export default firebase;