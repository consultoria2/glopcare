import { createApp } from 'vue'
import MainApp from './MainApp.vue'
import router from './router';
import store from './store';

import { IonicVue } from '@ionic/vue';
import { App, URLOpenListenerEvent } from '@capacitor/app';

import { createI18n } from 'vue-i18n';

/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css';
import '@ionic/vue/css/structure.css';
import '@ionic/vue/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/vue/css/padding.css';
import '@ionic/vue/css/float-elements.css';
import '@ionic/vue/css/text-alignment.css';
import '@ionic/vue/css/text-transformation.css';
import '@ionic/vue/css/flex-utils.css';
import '@ionic/vue/css/display.css';

/* Theme variables */
import './theme/variables.css';

/* Own components */
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import MazPhoneNumberInput from 'maz-ui/components/MazPhoneNumberInput';
import 'maz-ui/css/main.css';
import LvButton from 'lightvue/button';
import LvColorpicker from 'lightvue/color-picker';


import { SplashScreen } from '@capacitor/splash-screen';

import VueGoogleMaps from '@fawmi/vue-google-maps'
import VueGridLayout from "vue-grid-layout"

/* Mixins */
import StorageService from './storage';

import messages from './i18n'
import VueGtag from 'vue-gtag'

const i18n = createI18n({
  locale: 'es', // set locale
  fallbackLocale: 'en', // set fallback locale
  messages,
})

const app = createApp(MainApp)
  .use(IonicVue)
  .use(router)
  .use(store)
  .use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyDge5j5jvbNw3wLR8u29iDaxqSwMIAlV7Y',
        libraries: "places"
        // language: 'de',
    },
  })
  .use(VueGtag, {
    config: { id: 'G-WK11LJ8FXG' }
  }, router)
  .use(i18n)
  .use(VueGridLayout)
  .mixin(StorageService);

app.component('LoadingModal', Loading);
app.component('MazPhoneNumberInput', MazPhoneNumberInput);
app.component('LvButton', LvButton);
app.component('LvColorpicker', LvColorpicker);

// app.directive('resize',
//     (el, binding) => {
//       const onResizeCallback = binding.value;
//       window.addEventListener('resize', () => {
//         const width = document.documentElement.clientWidth;
//         const height = document.documentElement.clientHeight;
//         console.log('onResize', width, height);
//         onResizeCallback({ width, height });
//       });
//     }
// );
  
router.isReady().then(async () => {
  app.mount('#app');

  App.addListener('appUrlOpen', function (event: URLOpenListenerEvent) {
    // Example url: https://beerswift.app/tabs/tabs2
    // slug = /tabs/tabs2
    const slug = event.url.split('.com/app#').pop();
  
    // We only push to the route if there is a slug present
    if (slug) {
      router.push({
        path: slug,
      });
    }
    console.log('From Main:', slug);
  });

  SplashScreen.hide();
});