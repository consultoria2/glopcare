package com.glopcare;
import ch.byrds.capacitor.contacts.Contacts;
import com.codetrixstudio.capacitor.GoogleAuth.GoogleAuth;
import android.os.Bundle;

import com.getcapacitor.BridgeActivity;

public class MainActivity extends BridgeActivity {
    @Override
    public void onCreate(Bundle savedInstanceState){
        this.registerPlugin(Contacts.class);
        this.registerPlugin(com.getcapacitor.community.facebooklogin.FacebookLogin.class);
        this.registerPlugin(GoogleAuth.class);
        super.onCreate(savedInstanceState);
    }
}
    